#include "SkyboxObject.h"

void SkyboxObject::init(SkyboxShaderProgram* program)
{
		this->program = program;
}

void SkyboxObject::initGeometry()
{

	geometry = new ObjectGeometry();
	static const float screenCoords[] = {
	  -1.0f, -1.0f,
	   1.0f, -1.0f,
	  -1.0f,  1.0f,
	   1.0f,  1.0f
	};

	glGenVertexArrays(1, &(geometry->vertexArrayObject));
	glBindVertexArray(geometry->vertexArrayObject);
	
	// buffer for far plane rendering
	glGenBuffers(1, &(geometry->vertexBufferObject));
	glBindBuffer(GL_ARRAY_BUFFER, geometry->vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(screenCoords), screenCoords, GL_STATIC_DRAW);

	glEnableVertexAttribArray(program->screenCoordLocation);
	glVertexAttribPointer(program->screenCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
	glUseProgram(0);
	

	geometry->numTriangles = 2;

	glActiveTexture(GL_TEXTURE0);

	glGenTextures(1, &(geometry->texture));
	glBindTexture(GL_TEXTURE_CUBE_MAP, geometry->texture);

	const char* suffixes[] = { "px", "nx", "py", "ny", "pz", "nz" };
	GLuint targets[] = {
	  GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
	  GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
	  GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};
	for (int i = 0; i < 6; i++) {
		std::string texName = std::string(SKYBOX_CUBE_TEXTURE_FILE_PREFIX) + "_" + suffixes[i] + ".png";
		std::cout << "Loading cube map texture: " << texName << std::endl;
		if (!pgr::loadTexImage2D(texName, targets[i])) {
			pgr::dieWithError("Skybox cube map loading failed!");
		}
	}

	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	CHECK_GL_ERROR();
}

void SkyboxObject::draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix)
{
	glUseProgram(program->program);

	// create view rotation matrix by using view matrix with cleared translation
	glm::mat4 viewRotation = glm::mat4(1.0f);//viewMatrix;
	
	
	if(!followCamera)
		viewRotation = viewMatrix;
	viewRotation[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	// vertex shader will translate screen space coordinates (NDC) using inverse PV matrix
	glm::mat4 inversePVmatrix = glm::inverse(projectionMatrix * viewRotation);

	glUniformMatrix4fv(program->inversePVmatrixLocation, 1, GL_FALSE, glm::value_ptr(inversePVmatrix));
	glUniform1i(program->skyboxSamplerLocation, 0);

	// draw "skybox" rendering 2 triangles covering the far plane
	glBindVertexArray(geometry->vertexArrayObject);
	glBindTexture(GL_TEXTURE_CUBE_MAP, geometry->texture);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, geometry->numTriangles + 2);

	glBindVertexArray(0);
	glUseProgram(0);
}

