#include "CityBlock.h"

CityBlock::CityBlock(pgr::sg::SceneNode* parent, const char* fileName, const glm::vec3& offset, const int& id, const char* nodeName = nullptr)
	: AbstractObject(parent, fileName, id, nodeName) {
	int rotateAngle = (rand() % 4) * 90;
	printf("%d\n", rotateAngle);
	
	_transformNode->scale(glm::vec3(CITY_SCALE_FACTOR));

	_transformNode->translate(offset + glm::vec3(0.0f, 0.51f, 0.0f));
	_transformNode->rotate(glm::radians((float)rotateAngle), glm::vec3(0.0f, 1.0f, 0.0f));

	setLights();
	}

void CityBlock::setOffset(const glm::vec3& offset)
{
	_transformNode->translate(offset);
}

void CityBlock::setLights()
{
	LightObject sun;
	sun.name = "sun";
		sun.ambient = glm::vec3(0.5f, 0.5f, 0.5f);
		sun.diffuse = glm::vec3(1.0f);
		sun.specular = glm::vec3(0.5f);
		sun.position = glm::vec3(0.5f, 1.0f, 0.5f);
	_node->m_local_program->addLight(sun);
	//_node->m_local_program->addLight(sun);
	_node->m_local_program->initLocations();
}