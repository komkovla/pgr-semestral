#pragma once

#include "sceneGraph/SceneNode.h"
#include "object.h"
#include "common.h"
#include "render_stuff.h"
#include "data.h"
#include "barrel.h"

class BarrelObjectNode : public pgr::sg::SceneNode
{
public:
	BarrelObjectNode(ShaderProgram* program, const char* name = "Barrel", pgr::sg::SceneNode* parent = NULL);
	~BarrelObjectNode() {}
	void draw(const glm::mat4& view_matrix, const glm::mat4& projection_matrix) override;
	void init();
	void initModelMatrix();
	void initGeometry();
	
protected:
	ShaderProgram* program;
	ObjectGeometry * geometry;
};

