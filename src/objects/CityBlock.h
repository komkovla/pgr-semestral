#pragma once

//pgr


//my
#include "AbstractObject.h"



class CityBlock : public AbstractObject{
public:
	CityBlock(pgr::sg::SceneNode* parent, const char* fileName, const glm::vec3 & offset, const int & id, const char * nodeName);
	void setOffset(const glm::vec3& offset);
	void setLights() override;
private:
	glm::vec3 _offset;
	
};
