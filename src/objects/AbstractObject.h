#pragma once

//pgr
#include "pgr.h"
#include "SceneGraph/TransformNode.h"
#include "SceneGraph/MeshGeometry.h"

//my
#include "MeshNodeExt.h"
#include <object.h>

class AbstractObject {
public:
	AbstractObject(pgr::sg::SceneNode* parent, const char* fileName, const int& id, const char* nodeName);
	void setTransform(const glm::vec3& translate = glm::vec3(0.0f), const glm::vec3& scale = glm::vec3(1.0f), const glm::vec3& rotateAxis = glm::vec3(0.0f, 0.0f, 1.0f), const float& rotateAngle = 0.0f);
	virtual void setLights();
protected:
	int _id;
	std::string _name;
	pgr::sg::TransformNode* _transformNode;
	MeshNodeExt* _node;
	glm::vec3 _position;
	glm::vec3 _direction;
	
};