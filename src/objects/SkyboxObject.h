#pragma once

#include "pgr.h"
#include "sceneGraph/SceneNode.h"
#include "object.h"
#include "common.h"
#include <iostream>

class SkyboxObject : pgr::sg::SceneNode {
public:

	SkyboxObject(const char* name, pgr::sg::SceneNode* parent) : pgr::sg::SceneNode(name, parent) {}

	void init(SkyboxShaderProgram* program);
	void initGeometry();
	void draw(const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix) override;
	SkyboxShaderProgram* program;
	ObjectGeometry* geometry;
	bool followCamera = false;
};

