#include "AbstractObject.h"

AbstractObject::AbstractObject(pgr::sg::SceneNode* parent, const char* fileName, const int& id, const char* nodeName)
{
	if (nodeName)
		_name = nodeName;
	else {
		_name = fileName;
	}
	_transformNode = new pgr::sg::TransformNode(_name + "Transform", parent);
	pgr::sg::MeshGeometry* mesh_p = nullptr;
	if (!pgr::sg::MeshManager::Instance()->exists(fileName))
		pgr::sg::MeshManager::Instance()->insert(fileName, pgr::sg::MeshGeometry::LoadFromFile(fileName));
	mesh_p = pgr::sg::MeshManager::Instance()->get(fileName);
	if (!mesh_p) {
		printf("can't load: %s\n", fileName);
	}
	else {
		_node = new MeshNodeExt(_name, _transformNode);
		_node->setGeometry(mesh_p);
	}
}

void AbstractObject::setTransform(const glm::vec3& translate, const glm::vec3& scale, const glm::vec3& rotateAxis, const float& rotateAngle)
{
	printf("transform\n");
	_transformNode->translate(translate);
	_transformNode->scale(scale);
	_transformNode->rotate(rotateAngle, rotateAxis);
}

void AbstractObject::setLights()
{
}
