#include "BarrelObjectNode.h"

//void setTransformUniforms(ShaderProgram* commonShaderProgram, const glm::mat4& modelMatrix, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix);

BarrelObjectNode::BarrelObjectNode(ShaderProgram* program, const char* name, pgr::sg::SceneNode* parent) : pgr::sg::SceneNode(name, parent), program(program)
{
	geometry = new ObjectGeometry();
	geometry->VertArr = barrelVertices;
	geometry->FaceArr = barrelTriangles;
	geometry->numTriangles = barrelNTriangles;
	geometry->numVert = barrelNVertices;
	geometry->texture = pgr::createTexture(BARREL_TEX_FILENAME);
	initGeometry();
	initModelMatrix();
}

void BarrelObjectNode::init() {
	
}

void BarrelObjectNode::initModelMatrix()
{
	m_local_mat = glm::mat4(1.0f);
	m_local_mat = glm::translate(m_local_mat, glm::vec3(-1.0f, -0.9f, 0.1f));
	m_local_mat = glm::scale(m_local_mat, glm::vec3(0.1f));
}


void BarrelObjectNode::draw(const glm::mat4& view_matrix, const glm::mat4& projection_matrix)
{

	glUseProgram(program->program);
	glm::mat4 PVM =   projection_matrix * view_matrix * m_local_mat;
	//glUniform2fv(program->locations.uvPosition, 0);
	glUniformMatrix4fv(program->locations.PVMmatrix, 1, GL_FALSE, glm::value_ptr(PVM));
	glBindTexture(GL_TEXTURE_2D, geometry->texture);
	CHECK_GL_ERROR();
	glBindVertexArray(geometry->vertexArrayObject);
	glDrawElements(GL_TRIANGLES, geometry->numTriangles * 3, GL_UNSIGNED_INT, 0);
	//glDrawArrays(GL_TRIANGLES, 0, 3 * geometry->numTriangles);
	CHECK_GL_ERROR();
	pgr::sg::SceneNode::draw(view_matrix, projection_matrix);
	glBindVertexArray(0);
	
	return;
}



void BarrelObjectNode::initGeometry() {
	//vertexArray
	glGenVertexArrays(1, &(geometry->vertexArrayObject));
	glBindVertexArray(geometry->vertexArrayObject);
	CHECK_GL_ERROR();
	//Buffers
	glGenBuffers(1, &(geometry->vertexBufferObject));
	glBindBuffer(GL_ARRAY_BUFFER, geometry->vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, geometry->numVert * 8 * sizeof(float), geometry->VertArr, GL_STATIC_DRAW);
	CHECK_GL_ERROR();
	
	glGenBuffers(1, &(geometry->elementBufferObject));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->elementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, geometry->numTriangles * 3 * sizeof(unsigned int), geometry->FaceArr, GL_STATIC_DRAW);
	CHECK_GL_ERROR();


	//AttribArray
	//Pos
	glEnableVertexAttribArray(program->locations.pos);
	glVertexAttribPointer(program->locations.pos, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), 0);
	CHECK_GL_ERROR();
	//Normal
	/*glEnableVertexAttribArray(program->locations.normal);
	glVertexAttribPointer(program->locations.normal, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	CHECK_GL_ERROR();*/
	//UV
	glEnableVertexAttribArray(program->locations.uvPosition);
	glVertexAttribPointer(program->locations.uvPosition, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	CHECK_GL_ERROR();
	glBindVertexArray(0);
	CHECK_GL_ERROR();
}



