#include "CarObject.h"



CarObject::CarObject(pgr::sg::SceneNode* parent, const char* fileName, const int& id, const char* nodeName = nullptr)
	: AbstractObject(parent, fileName, id, nodeName) 
{
	_direction = glm::vec3(0.0f);
	setLights();
}

void CarObject::setLights()
{
	//_node->m_local_program->addLight("reflectorLeft");
	//_node->m_local_program->initLocations();
}
