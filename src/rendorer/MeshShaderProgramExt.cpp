#include "MeshShaderProgramExt.h"


MeshShaderProgramExt::MeshShaderProgramExt(GLuint prId){
	_meshShaderProgram = new pgr::sg::MeshShaderProgram(prId);
	
}

MeshShaderProgramExt::MeshShaderProgramExt(pgr::sg::MeshShaderProgram* program)
{
	_meshShaderProgram = program;
}

void MeshShaderProgramExt::addLight(LightObject light)
{
	m_lights.push_back(light);
}

void MeshShaderProgramExt::initLocations() {
	_meshShaderProgram->initLocations();
	GLuint programId = _meshShaderProgram->m_programId;
	//light locations;
	for (LightObject & light : m_lights) {
		light.locations.m_programId = programId;
		light.locations.m_ambient = glGetUniformLocation(programId, (light.name + ".ambient").c_str());
		light.locations.m_diffuse = glGetUniformLocation(programId, (light.name + ".diffuse").c_str());
		light.locations.m_specular = glGetUniformLocation(programId, (light.name + ".specular").c_str());
		light.locations.m_spotCosCutOff = glGetUniformLocation(programId, (light.name + ".spotCosCutOff").c_str());
		light.locations.m_spotDirection = glGetUniformLocation(programId, (light.name + ".spotDirection").c_str());
		light.locations.m_spotExponent = glGetUniformLocation(programId, (light.name + ".spotExponent").c_str());
		light.locations.m_position = glGetUniformLocation(programId, (light.name + ".position").c_str());
	}
	CHECK_GL_ERROR();
}


void MeshShaderProgramExt::updateUniforms(pgr::sg::SceneNode* nd) {
	_meshShaderProgram->updateUniforms(nd);
}