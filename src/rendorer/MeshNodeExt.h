#pragma once

//pgr deps
#include "pgr.h"
#include "sceneGraph/SceneNode.h"
#include "sceneGraph/MeshNode.h"
#include "sceneGraph/MeshGeometry.h"
#include "sceneGraph/Resources.h"
#include "../common/common.h"

//my deps
#include "MeshShaderProgramExt.h"


/// manages rendering of a MeshGeometry
class MeshNodeExt : public pgr::sg::MeshNode {
public:
	MeshNodeExt(const std::string& name = "<MeshNode>", pgr::sg::SceneNode* parent = NULL);

	/// reimplemented draw
	void draw(const glm::mat4& view_matrix, const glm::mat4& projection_matrix) override;
	MeshShaderProgramExt * m_local_program;
protected:
	/// creates shader
	void loadProgram() override;
};