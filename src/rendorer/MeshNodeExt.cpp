#include "MeshNodeExt.h"




MeshNodeExt::MeshNodeExt(const std::string& name, SceneNode* parent) : pgr::sg::MeshNode(name, parent) {
    
}



void MeshNodeExt::loadProgram() {
    if (m_local_program)
        pgr::sg::ShaderManager::Instance()->release("MeshNode-shader");

    if (!pgr::sg::ShaderManager::Instance()->exists("MeshNode-shader")) {
        std::string shaderPath = LIGHTING_SHADER_PATH;
        
        GLuint shaderList[] = {
          pgr::createShaderFromFile(GL_VERTEX_SHADER,    shaderPath + ".vert" ),
          pgr::createShaderFromFile(GL_FRAGMENT_SHADER, shaderPath + ".frag"),
          //pgr::createShaderFromFile(GL_VERTEX_SHADER,   pgr::frameworkRoot() + "data/sceneGraph/MeshNode.vert"),
          //pgr::createShaderFromFile(GL_FRAGMENT_SHADER, pgr::frameworkRoot() + "data/sceneGraph/MeshNode.frag"),
          0
        };
        //m_local_program = new MeshShaderProgram( pgr::createProgram(shaderList) );
        m_local_program = new MeshShaderProgramExt(pgr::createProgram(shaderList));
        if (m_local_program->meshShaderProgram()->m_programId == 0) {
            printf("Can't create program\n");
            return;
        }

        pgr::sg::ShaderManager::Instance()->insert("MeshNode-shader", m_local_program->meshShaderProgram());
    }
    else {
        m_program = dynamic_cast<pgr::sg::MeshShaderProgram*>(pgr::sg::ShaderManager::Instance()->get("MeshNode-shader"));
        m_local_program = new MeshShaderProgramExt(m_program);
    }
    m_local_program->initLocations();
    m_program = m_local_program->meshShaderProgram();
}


void MeshNodeExt::draw(const glm::mat4& view_matrix, const glm::mat4& projection_matrix)
{
     glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

     // inherited draw - draws all children
     SceneNode::draw(view_matrix, projection_matrix);

     glm::mat4 PVMmatrix = projection_matrix * view_matrix * globalMatrix();
     glm::mat4 Vmatrix = view_matrix;
     glm::mat4 Mmatrix = globalMatrix();

     glUseProgram(m_program->m_programId);

     glUniformMatrix4fv(m_program->m_PVMmatrix, 1, GL_FALSE, glm::value_ptr(PVMmatrix));			// model-view-projection
     glUniformMatrix4fv(m_program->m_Vmatrix, 1, GL_FALSE, glm::value_ptr(Vmatrix));			    // view
     glUniformMatrix4fv(m_program->m_Mmatrix, 1, GL_FALSE, glm::value_ptr(Mmatrix));			    // model
     //glm::mat4 NormalMatrix = glm::transpose(glm::inverse(Vmatrix * Mmatrix));			            // should be this way, but inverse returns bad matrix
     //glm::mat4 NormalMatrix = glm::transpose(glm::inverse(Vmatrix));			            // should be this way, but inverse returns bad matrix
     
     const glm::mat4 modelRotationMatrix = glm::mat4(
         Mmatrix[0],
         Mmatrix[1],
         Mmatrix[2],
         glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
     );
     glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelRotationMatrix));
     
     
     glUniformMatrix4fv(m_program->m_NormalMatrix, 1, GL_FALSE, glm::value_ptr(modelRotationMatrix));      // correct matrix for non-rigid transf


     glm::vec3 sunPosition = glm::vec3(Vmatrix * Mmatrix * glm::vec4(0.5f, 1.0f, 0.5f, 0.0f));      // sun to eye
     //printf("sun: x:%f y:%f z:%f\n", sunPosition.x, sunPosition.y, sunPosition.z);
     //for (LightObject * light : m_local_program->lightShaderProgram()) {
     //    //printf("add_lights\n");
     //   glUniform3fv(light->locations.m_diffuse,  1, glm::value_ptr(light->diffuse));
     //   glUniform3fv(light->locations.m_ambient,  1, glm::value_ptr(light->ambient));
     //   glUniform3fv(light->locations.m_specular, 1, glm::value_ptr(light->specular));
     //   glUniform3fv(light->locations.m_position, 1, glm::value_ptr(sunPosition));
     //}
     for (int i = 0; i < m_local_program->m_lights.size(); i++) {
         LightObject light = m_local_program->m_lights[i];
         glUniform3fv(light.locations.m_diffuse, 1, glm::value_ptr(light.diffuse));
         glUniform3fv(light.locations.m_ambient, 1, glm::value_ptr(light.ambient));
         glUniform3fv(light.locations.m_specular, 1, glm::value_ptr(light.specular));
         glUniform3fv(light.locations.m_position, 1, glm::value_ptr(sunPosition));
     }
     
     glUniform1f(m_program->m_time, (float)m_time);        // in seconds

     //glUniform1i(m_texSamplerID, 0);

     glBindVertexArray(m_vertexArrayObject);

     // draw all submeshes = all material groups from SubMeshList
     pgr::sg::MeshGeometry::SubMesh * subMesh_p = NULL;

     for (unsigned mat = 0; mat < m_mesh->getSubMeshCount(); mat++) {

         subMesh_p = m_mesh->getSubMesh(mat);

         glUniform3fv(m_program->m_diffuse, 1, subMesh_p->diffuse);  // 2nd parameter must be 1 - it declares number of vectors in the vector array
         glUniform3fv(m_program->m_ambient, 1, subMesh_p->ambient);
         glUniform3fv(m_program->m_specular, 1, subMesh_p->specular);
         glUniform1f(m_program->m_shininess, subMesh_p->shininess);

         if (subMesh_p->textureID != 0 && m_mesh->hasTexCoords() == true) {
             glUniform1i(m_program->m_useTexture, 1);		 // do texture sampling

             glUniform1i(m_program->m_texSampler, 0);  // texturing unit 0 -> samplerID   [for the GPU linker]
             glActiveTexture(GL_TEXTURE0 + 0);  // texturing unit 0 -> to be bound [for OpenGL BindTexture]
             glBindTexture(GL_TEXTURE_2D, subMesh_p->textureID);
         }
         else {
             glUniform1i(m_program->m_useTexture, 0);		// do not sample the texture
         }

         //glDrawElements( GL_TRIANGLES, subMesh_p->nIndices, GL_UNSIGNED_INT, (void *) (subMesh_p->startIndex * sizeof(unsigned int)));
         // base vertex must be added to the indices for each block (as they are rellative inside the submesh and start from 0)
         // do it in Resources::Load() and use DrawElements, or use glDrawElementsBaseVertex
         glDrawElementsBaseVertex(GL_TRIANGLES, subMesh_p->nIndices, GL_UNSIGNED_INT, (void*)(subMesh_p->startIndex * sizeof(unsigned int)), subMesh_p->baseVertex);
     }

     glBindVertexArray(0);
}