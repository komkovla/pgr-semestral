#pragma once

#include "pgr.h"
#include "sceneGraph/ShaderProgram.h"
#include "LightShaderProgram.h"






class MeshShaderProgramExt {
public:
	MeshShaderProgramExt(GLuint prId);
	MeshShaderProgramExt(pgr::sg::MeshShaderProgram * program);
	void addLight(LightObject light);
	void initLocations();
	void updateUniforms(pgr::sg::SceneNode* nd);

	void setMeshShaderProgram(pgr::sg::MeshShaderProgram * meshShaderProgram) {
		_meshShaderProgram = meshShaderProgram;
	}
	pgr::sg::MeshShaderProgram * meshShaderProgram(void) {
		return _meshShaderProgram;
	}
	std::vector <LightObject> lightShaderProgram(void) {
		return m_lights;
	}

	std::vector <LightObject> m_lights;
private:
	pgr::sg::MeshShaderProgram * _meshShaderProgram;

};
