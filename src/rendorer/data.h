#pragma once


namespace data {
	
	const std::string colorVertexShaderSrc(
		"#version 140\n"
		"uniform mat4 PVMmatrix;\n"
		"in vec3 position;\n"
		"in vec3 color;\n"
		"smooth out vec4 theColor;\n"
		"void main() {\n"
		"  gl_Position = PVMmatrix * vec4(position, 1.0);\n"
		"  theColor = vec4(color, 1.0);\n"
		"}\n"
	);

	const std::string colorFragmentShaderSrc(
		"#version 140\n"
		"smooth in vec4 theColor;\n"
		"out vec4 outputColor;\n"
		"void main() {\n"
		"  outputColor = theColor;\n"
		"}\n"
	);



	const std::string textureVertexShaderSrc(
		"#version 140\n"
		"uniform mat4 PVMmatrix;\n"
		"in  vec2 uvPosition;\n"
		"out vec2 uvPosition_v;\n"
		"in  vec3 position;\n"
		"in  vec3 normal;\n"
		"smooth out vec4 theColor;\n"
		"void main() {\n"
		"  gl_Position = PVMmatrix * vec4(position, 1.0);\n"
		"  uvPosition_v = uvPosition;\n"
		"}\n"
	);
	const std::string textureFragmentShaderSrc(
		"#version 140\n"
		"smooth in  vec2 uvPosition_v;\n"
		"uniform    sampler2D tex;\n"
		"out        vec4 color_f;\n"
		"void main() {\n"
		"  color_f = texture(tex, uvPosition_v);\n"
		"}\n"
	);


}


