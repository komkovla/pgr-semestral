#include "render_stuff.h"







void initCommonShader(ShaderProgram * shaderProgram) {
	std::vector<GLuint> shaderList;

	//common_shader
	shaderList.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, data::colorVertexShaderSrc));
	shaderList.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, data::colorFragmentShaderSrc));
	shaderProgram->program = pgr::createProgram(shaderList);

	// get attrib locations
	shaderProgram->locations.pos =					glGetAttribLocation(shaderProgram->program, "position");
	shaderProgram->locations.col =					glGetAttribLocation(shaderProgram->program, "color");
	// get uniforms locations
	shaderProgram->locations.PVMmatrix =			glGetUniformLocation(shaderProgram->program, "PVMmatrix");
}

void initTextureShader(ShaderProgram* shaderProgram) {
	std::vector<GLuint> shaderList;

	//common_shader
	shaderList.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, data::textureVertexShaderSrc));
	shaderList.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, data::textureFragmentShaderSrc));
	shaderProgram->program = pgr::createProgram(shaderList);

	// get attrib locations
	shaderProgram->locations.pos = glGetAttribLocation(shaderProgram->program, "position");
	shaderProgram->locations.normal = glGetAttribLocation(shaderProgram->program, "normal");
	shaderProgram->locations.uvPosition = glGetAttribLocation(shaderProgram->program, "uvPosition");

	// get uniforms locations
	shaderProgram->locations.PVMmatrix = glGetUniformLocation(shaderProgram->program, "PVMmatrix");
	shaderProgram->locations.tex = glGetUniformLocation(shaderProgram->program, "tex");
	CHECK_GL_ERROR();
};

void initSkyboxShader(SkyboxShaderProgram* shaderProgram)
{
	std::vector<GLuint> shaderList;
	shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "skyBox.vert"));
	shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "skyBox.frag"));

	// create the program with two shaders
	shaderProgram->program = pgr::createProgram(shaderList);

	// handles to vertex attributes locations
	shaderProgram->screenCoordLocation = glGetAttribLocation(shaderProgram->program, "screenCoord");
	// get uniforms locations
	shaderProgram->skyboxSamplerLocation = glGetUniformLocation(shaderProgram->program, "skyboxSampler");
	shaderProgram->inversePVmatrixLocation = glGetUniformLocation(shaderProgram->program, "inversePVmatrix");
};
