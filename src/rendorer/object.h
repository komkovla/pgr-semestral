#pragma once

#include "pgr.h"
#include "sceneGraph/ShaderProgram.h"
#include "data.h"
/**
 * \brief Shader program related stuff (id, locations, ...).
 */
typedef struct _ShaderProgram {
	// identifier for the shader program
	GLuint program;          // = 0;

	struct {
		
		// vertex attributes locations
		GLint pos;				// = -1;
		GLint col;				// = -1;
		GLint normal;			// = -1;

		// uniforms locations
		GLint PVMmatrix;		// = -1;
		GLint uvPosition;		// = -1;
		GLint tex;				// = -1;
	} locations;

	// ...
}ShaderProgram;

typedef struct _SkyboxShaderProgram {
	// identifier for the shader program
	GLuint program;                 // = 0;
	// vertex attributes locations
	GLint screenCoordLocation;      // = -1;
	// uniforms locations
	GLint inversePVmatrixLocation; // = -1;
	GLint skyboxSamplerLocation;    // = -1;
}SkyboxShaderProgram;

typedef struct _LightObject {
	std::string name;
	glm::vec3  ambient;
	glm::vec3  diffuse;
	glm::vec3  specular;
	glm::vec3  position;
	glm::vec3  spotDirection;
	float spotCosCutOff;
	float spotExponent;
	struct {
		GLuint m_programId;
		GLint m_ambient;
		GLint m_diffuse;
		GLint m_specular;
		GLint m_position;
		GLint m_spotDirection;
		GLint m_spotCosCutOff;
		GLint m_spotExponent;
	} locations;
} LightObject;



/**
 * \brief Geometry of an object (vertices, triangles).
 */
typedef struct _ObjectGeometry {
	GLuint        vertexBufferObject;   // identifier for the vertex buffer object
	GLuint        elementBufferObject;  // identifier for the element buffer object
	GLuint        vertexArrayObject;    // identifier for the vertex array object
	unsigned int  numTriangles;         // number of triangles in the mesh
	unsigned int  numVert;				// number of verticies in the mesh
	const float* VertArr;
	const unsigned int* FaceArr;
	glm::vec3     ambient;
	glm::vec3     diffuse;
	glm::vec3     specular;
	float         shininess;
	GLuint        texture;
}ObjectGeometry;







