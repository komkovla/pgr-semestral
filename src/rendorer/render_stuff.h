#pragma once

#include "pgr.h"
#include "object.h"
#include "data.h"




void initCommonShader(ShaderProgram * shaderProgram);
void initTextureShader(ShaderProgram* shaderProgram);
void initSkyboxShader(SkyboxShaderProgram * shaderProgram);

