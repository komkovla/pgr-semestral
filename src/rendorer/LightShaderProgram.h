#pragma once


#include "pgr.h"
#include "sceneGraph/SceneNode.h"
#include "object.h"

class LightShaderProgram {
public:
	LightShaderProgram(GLuint prId);
	void initLocations();

	void updateUniforms(pgr::sg::SceneNode* nd);

private:
	LightObject * _lightObject;
};