#pragma once
#include "pgr.h"
#include "common.h"



enum CameraMode {
	fly,
	static1,
	static2,
	dynamic_object,
	dynamic_curve,
	modes_count,

};

class Camera
{
public:
	void init();
	void setCamera(CameraMode mode);
	void setDirection();
	void nextCamera();
	CameraMode mode = static1;
	CameraMode mode_old = static1;
	glm::mat4 getViewMatrix();
	glm::vec3 eye;
	glm::vec3 center;
	glm::vec3 up;
	glm::vec3 direction;
	glm::vec3 position = glm::vec3(0.0f);
	float elevationAngle = 0.0f;	// in degreees
	float rotateAngle = 0.0f;		// in degrees
	float delta = CAMERA_VIEW_SPEED_DELTA;
	void turnLeft();
	void turnRight();
	void goForward();
	void goBackward();
	bool is_check_border = false;
	bool check_borders(glm::vec3 pos);
	float ROAD_WIDTH = 1.55f;
	float ROAD_LENGTH = 6.5f;
	float ROAD_SECOND_LENGTH = 2.0f;
	float ROAD_OFFSET_Z = -0.74f;
	//float ROAD_SECOND_OFFSET_X = -1.74f;
	float ROAD_OFFSET_Y = 0.1f;
	bool isWalk = false;
};

