#include "Camera.h"

void Camera::init()
{
	position = glm::vec3(0.0f);
	mode = START_CAMERA_MODE;
	elevationAngle = 0.0f;
	rotateAngle = 0.0f;
}
void Camera::setCamera(CameraMode mode)
{
	switch (mode)
	{
	case fly:
	{
		setCamera(mode_old);
		#ifdef DEBUG
			printf("eye = x: %f, y: %f, z: %f\n", eye.x, eye.y, eye.z);
		#endif // DEBUG

		setDirection();
		if (isWalk) {
			eye += glm::vec3(position.x, 0.0f, position.z);
		}
		else {
			eye += position;
		}
		glm::vec3 rotationAxis_y = glm::cross(direction, up);
		glm::mat4 cameraTransform_y = glm::rotate(glm::mat4(1.0f), glm::radians(elevationAngle), rotationAxis_y);
		glm::mat4 cameraTransform_x = glm::rotate(glm::mat4(1.0f), glm::radians(rotateAngle), up);
		up		=				glm::vec3(cameraTransform_y * glm::vec4(up, 0.0f));
		up		=				glm::vec3(cameraTransform_x * glm::vec4(up, 0.0f));
		direction =				glm::vec3(cameraTransform_y * glm::vec4(direction, 0.0f));
		direction =				glm::vec3(cameraTransform_x * glm::vec4(direction, 0.0f));
		center = eye + direction;
	}
		break;
	case static1:
		eye = glm::vec3(1.0f, 1.0f, -0.5f);
		center = glm::vec3(0.0f, 0.0f, 0.0f);
		up = glm::vec3(0.0f, 0.5f, 0.0f);
		break;
	case static2:
		eye = glm::vec3(1.0f, 0.5f, -0.5f);
		center = glm::vec3(0.0f, 0.0f, 0.0f);
		up = glm::vec3(0.0f, 0.5f, 0.0f);
		break;
	case dynamic_object:
		eye = glm::vec3(1.0f, 0.5f, -0.5f);
		center = glm::vec3(0.0f, 0.0f, 0.0f);
		up = glm::vec3(0.0f, 0.5f, 0.0f);
		break;
	case dynamic_curve:
		eye = glm::vec3(1.0f, 0.5f, -0.5f);
		center = glm::vec3(0.0f, 0.0f, 0.0f);
		up = glm::vec3(0.0f, 0.5f, 0.0f);
		break;
	case modes_count:
		break;
	default:
		break;
	}
	
}
void Camera::nextCamera()
{
	mode_old = mode;
	mode = static_cast<CameraMode>((mode + 1) % modes_count);
	if (mode == fly) {
		position = glm::vec3(0.0f);
	}
}
glm::mat4 Camera::getViewMatrix()
{
	setCamera(mode);
	return glm::lookAt(eye, center, up);
}

void Camera::setDirection() {
	direction = center - eye;

}


bool Camera::check_borders(glm::vec3 pos) {
	glm::vec3 center(-0.67f, 0.0f, 0.13f);
	pos += center;
	//second road check
	if ( abs(pos.x) < (ROAD_WIDTH / 2) ) {
		if (abs(pos.z) > ROAD_WIDTH + 2.0f / 2)
			return false;
		return true;

	}
	
	//main road check

	//z
	if (abs(pos.z + ROAD_OFFSET_Z) > ROAD_WIDTH / 2)
		return false;
	//x
	if (abs(pos.x) > ROAD_LENGTH)
		return false;

	//y

	return true;
}

void Camera::turnLeft()
{
	glm::vec3 new_position = position - delta * glm::normalize(glm::cross(direction, up));
	if (is_check_border) {
		if (check_borders(new_position)) {
			position = new_position;
		}
	}
	else
		position = new_position;

	
}

void Camera::turnRight()
{
	glm::vec3 new_position = position + delta * glm::normalize(glm::cross(direction, up));
	if (is_check_border) {
		if (check_borders(new_position)) {
			position = new_position;
		}
	}
	else
		position = new_position;
	//position += delta * glm::normalize(glm::cross(direction, up));

}

void Camera::goForward()
{
	glm::vec3 new_position = position + delta * glm::normalize(direction);
	if (is_check_border) {
		if (check_borders(new_position)) {
			position = new_position;
		}
	}
	else
		position = new_position;
}

void Camera::goBackward()
{
	glm::vec3 new_position = position - delta * glm::normalize(direction);
	if (is_check_border) {
		if (check_borders(new_position)) {
			position = new_position;
		}
	}
	else
		position = new_position;
}

