#pragma once




#define TERRAIN_FILE_NAME "./data/terrain"
#define CITY_FILE_NAME "./data/city/city_block.fbx"
#define CITY_SCALE_FACTOR 5.0f
//#define CAR_FILE_NAME "./data/car/old_car_ford_1948_FBX_2016.fbx"
#define CAR_FILE_NAME "./data/car/car.obj"
#define CAMERA_ELEVATION_MAX 90.0f
#define MOUSE_SPEED 0.5f
#define SKYBOX_CUBE_TEXTURE_FILE_PREFIX "./data/cubemap/day"
#define BARREL_TEX_FILENAME "./data/RustTexture.png"
#define LIGHTING_SHADER_PATH "./src/shaders/lightingPerVertex"

/*
* 	fly,
	static1,
	static2,
	dynamic_object,
	dynamic_curve,
	modes_count,
*/
#define START_CAMERA_MODE static2
#define CAMERA_VIEW_SPEED_DELTA 0.01f
#define CAMERA_FASTER_VIEW_SPEED_DELTA 0.1f
//#define ROAD_WIDTH 1.0f
//#define ROAD_LENGTH 1.0f
//#define ROAD_OFFSET_Z -0.7f 
//#define ROAD_OFFSET_Y 0.1f



enum { KEY_LEFT_ARROW, KEY_RIGHT_ARROW, KEY_UP_ARROW, KEY_DOWN_ARROW, KEY_SPACE, KEY_SHIFT, KEYS_COUNT };
