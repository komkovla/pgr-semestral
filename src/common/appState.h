#pragma once
#include <string>
#include "common.h"


class AppState
{
public:
	void init();
	int window_width = 800;
	int window_height = 800;
	std::string windowTitle = "PGR: City Scape";
	bool keyMap[KEYS_COUNT];
	float time_elapsed = 0;
};

