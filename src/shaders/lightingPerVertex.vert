#version 140

struct Material {
  vec3  ambient;       
  vec3  diffuse;       
  vec3  specular;      
  float shininess;     
};

uniform bool  useTexture;   
uniform sampler2D texSampler; 

struct Light {         
  vec3  ambient;       
  vec3  diffuse;       
  vec3  specular;      
  vec3  position;      
  vec3  spotDirection; 
  float spotCosCutOff; 
  float spotExponent;  
};

in vec3 position;           // vertex position in world space
in vec3 normal;             // vertex normal
in vec2 texCoord;           // incoming texture coordinates

uniform float time;         // time used for simulation of moving lights (such as sun)
uniform Material material;  // current material
uniform Light sun;
uniform Light carReflector;

uniform mat4 PVMmatrix;     // Projection * View * Model  --> model to clip coordinates
uniform mat4 Vmatrix;       // View                       --> world to eye coordinates
uniform mat4 Mmatrix;       // Model                      --> model to world coordinates
uniform mat4 NormalMatrix;  // inverse transposed Mmatrix

uniform vec3 reflectorPosition;   // reflector position (world coordinates)
uniform vec3 reflectorDirection;  // reflector direction (world coordinates)

smooth out vec2 texCoord_v;  // outgoing texture coordinates
smooth out vec4 color_v;     // outgoing fragment color



vec4 directionalLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {

  
  vec3 L = normalize(light.position);
  vec3 R = reflect(-L, vertexNormal);
  vec3 V = normalize(-vertexPosition);
  float NdotL = max(0.0, dot(vertexNormal, L));
  float RdotV = max(0.0, dot(R, V));
    
  vec3 res = vec3(0.0);
  res += material.ambient * light.ambient;
  res += material.diffuse * light.diffuse; NdotL;
  res += material.specular * light.specular * pow(RdotV, material.shininess);

  return vec4(res, 1.0);
}

vec4 spotLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {

  vec3 ret = vec3(0.0);

  vec3 L = normalize(light.position - vertexPosition);
  vec3 R = reflect(-L, vertexNormal);
  vec3 V = normalize(-vertexPosition);
  
  float NdotL = max(0.0, dot(vertexNormal, L));
  float RdotV = max(0.0, dot(R, V));
  float spotCoef = max(0.0, dot(-L, light.spotDirection));

  ret += material.ambient * light.ambient;
  ret += material.diffuse * light.diffuse * NdotL;
  ret += material.specular * light.specular * pow(RdotV, material.shininess);

  if(spotCoef < light.spotCosCutOff)
    ret *= 0.0;
  else
    ret *= pow(spotCoef, light.spotExponent);

  return vec4(ret, 1.0);
}



void main() {

  //setupSun();
  // transform
  vec3 vertexPosition = (Vmatrix * Mmatrix * vec4(position, 0.0)).xyz;                      // vertex to eye
  vec3 vertexNormal   = normalize( (Vmatrix * NormalMatrix * vec4(normal, 0.0) ).xyz);      // normal to eye normalised

  //ambient
  vec3 globalAmbientLight = vec3(0.5f);
  vec4 outputColor = vec4(material.ambient + globalAmbientLight, 0.0);

  //texture + light light
  outputColor += directionalLight(sun, material, vertexPosition, vertexNormal);

  gl_Position = PVMmatrix * vec4(position, 1);   // out:v vertex in clip coordinates

  // outputs entering the fragment shader
  color_v = outputColor;
  texCoord_v = texCoord;
}
