#pragma once
//system
#include <iostream>

//pgr
#include "pgr.h"

//local
#include "object.h"
#include "appState.h"
#include "Camera.h"
#include "common.h"
#include "BarrelObjectNode.h"
#include "render_stuff.h"
#include "SkyboxObject.h"
#include "CityBlock.h"
#include "CarObject.h"


//global variables

ShaderProgram commonShaderProgram;
ShaderProgram TextureShaderProgram;
SkyboxShaderProgram skyBoxShaderProgram;
AppState appState;
Camera camera;
SkyboxObject* skybox;
std::vector<CityBlock> cityBlocks;
std::vector<CarObject> cars;


bool mouseRedisplay = false;;
pgr::sg::SceneNode* rootNode_p = nullptr;
glm::mat4 projectionMatrix;




void initBarrel(pgr::sg::SceneNode* parent, const char* name) {
	BarrelObjectNode * barrel = new BarrelObjectNode(&TextureShaderProgram,name, parent);
}

void initSkybox(pgr::sg::SceneNode* parent, const char* name) {
	skybox = new SkyboxObject(name, parent);
	skybox->init(&skyBoxShaderProgram);
	skybox->initGeometry();
}
void initLightObject(pgr::sg::SceneNode* parent, const char* name) {
	//LightObject* lightObject = new LightObject(&lightShaderProgram,name, parent);

}


void initializeScene() {

	// initialize random seed
	srand((unsigned int)time(NULL));

	//initLightShader(&lightShaderProgram);
	//initCommonShader(&commonShaderProgram);
	//initTextureShader(&TextureShaderProgram);
	//initSkyboxShader(&skyBoxShaderProgram);

	rootNode_p = new pgr::sg::SceneNode("root");

	pgr::sg::TransformNode* rootTransform = new pgr::sg::TransformNode("RootTransform", rootNode_p);

	cityBlocks.emplace_back(rootTransform, CITY_FILE_NAME, glm::vec3(0.0f, 0.0f, 0.0f), 0, "city");
	cars.emplace_back(rootTransform, CAR_FILE_NAME, 0, "car");
	cars[0].setTransform(glm::vec3(0.0f,0.0f,0.0f), glm::vec3(2.0f));

	//cityBlocks.emplace_back(transformNode_p, "./data/car/old_car_ford_1948_FBX_2016.fbx", glm::vec3(0.0f), 0, "car");
	cityBlocks.emplace_back(rootTransform, CITY_FILE_NAME, glm::vec3(2.0f,0.0f,0.0f), 0, "city");
	cityBlocks.emplace_back(rootTransform, CITY_FILE_NAME, glm::vec3(-2.0f,0.0f,0.0f), 0, "city");
	cityBlocks.emplace_back(rootTransform, CITY_FILE_NAME, glm::vec3(0.0f,0.0f,2.0f), 0, "city");
	cityBlocks.emplace_back(rootTransform, CITY_FILE_NAME, glm::vec3(0.0f,0.0f,-2.0f), 0, "city");
	//initBarrel(transformNode_p, "Barrel");
	//initSkybox(transformNode_p, "Skybox");
	//initLightObject(transformNode_p, "lightObject");
	rootNode_p->dump();
	
	return;
}



// -----------------------  Window callbacks ---------------------------------

/**
 * \brief Draw the window contents.
 */
void displayCb() {
	glClearColor(0.3f, 0.9f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	projectionMatrix = glm::perspective(glm::radians(60.0f), appState.window_width / (float) appState.window_height, 0.01f, 100.0f);
	if( camera.eye > )
	
	if (rootNode_p) {
		rootNode_p->draw(camera.getViewMatrix(), projectionMatrix);
	}
	CHECK_GL_ERROR();

	glutSwapBuffers();
}

/**
 * \brief Window was reshaped. 
 * \param newWidth New window width
 * \param newHeight New window height
 */
void reshapeCb(int width, int height) {
	// TODO: Take new window size and update the application state,
	// window and projection.
	float aspectRatio = (float)width / height;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	appState.window_width = width;
	appState.window_height = height;

	// perspective projection setup
	projectionMatrix = glm::perspective(60.0f, aspectRatio, 1.0f, 1000.0f);
	// glViewport(...);
};

// -----------------------  Keyboard ---------------------------------
void restart() {
	camera.init();
	appState.init();
}

/**
 * \brief Handle the key pressed event.
 * Called whenever a key on the keyboard was pressed. The key is given by the "keyPressed"
 * parameter, which is an ASCII character. It's often a good idea to have the escape key (ASCII value 27)
 * to call glutLeaveMainLoop() to exit the program.
 * \param keyPressed ASCII code of the key
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void passiveMouseMotionCb(int mouseX, int mouseY);
void keyboardCb(unsigned char keyPressed, int mouseX, int mouseY) {
	switch (keyPressed) {
	case 27: //esc
		glutLeaveMainLoop();
		break;
	case 'r':
		printf("keyboardCb: restart\n");
		restart();

		break;
	case 'c':
	{
		camera.nextCamera();
		if (camera.mode == fly) {
			glutPassiveMotionFunc(passiveMouseMotionCb);
			glutWarpPointer(appState.window_width / 2, appState.window_height / 2);
		}
		else {
			glutPassiveMotionFunc(NULL);
		}
		printf("keyboardCb: selected camera = %d\n", camera.mode);
	}
		break;
	case 'a':
		appState.keyMap[KEY_LEFT_ARROW] = true;
		break;
	case 's':
		appState.keyMap[KEY_DOWN_ARROW] = true;
		break;
	case 'd':
		appState.keyMap[KEY_RIGHT_ARROW] = true;
		break;
	case 'w':
		appState.keyMap[KEY_UP_ARROW] = true;
		break;
	default:
		break;
	}
	if (glutGetModifiers() == GLUT_ACTIVE_SHIFT)
		appState.keyMap[KEY_SHIFT] = true;
	glutPostRedisplay();
}

// Called whenever a key on the keyboard was released. The key is given by
// the "keyReleased" parameter, which is in ASCII. 
/**
 * \brief Handle the key released event.
 * \param keyReleased ASCII code of the released key
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void keyboardUpCb(unsigned char keyReleased, int mouseX, int mouseY) {
	switch (keyReleased) {
	case 'a':
		appState.keyMap[KEY_LEFT_ARROW] = false;
		break;
	case 's':
		appState.keyMap[KEY_DOWN_ARROW] = false;
		break;
	case 'd':
		appState.keyMap[KEY_RIGHT_ARROW] = false;
		break;
	case 'w':
		appState.keyMap[KEY_UP_ARROW] = false;
		break;
	default:
		break;
	}
	if (glutGetModifiers() != GLUT_ACTIVE_SHIFT)
		appState.keyMap[KEY_SHIFT] = false;
	glutPostRedisplay();

}

//
/**
 * \brief Handle the non-ASCII key pressed event (such as arrows of F1).
 *  The special keyboard callback is triggered when keyboard function (Fx) or directional
 *  keys are pressed.
 * \param specKeyPressed int value of a predefined glut constant such as GLUT_KEY_UP
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void specialKeyboardCb(int specKeyPressed, int mouseX, int mouseY) {
	switch (specKeyPressed) {
	case GLUT_KEY_UP:
		appState.keyMap[KEY_UP_ARROW] = true;
		break;
	case GLUT_KEY_DOWN:
		appState.keyMap[KEY_DOWN_ARROW] = true;
		break;
	case GLUT_KEY_LEFT:
		appState.keyMap[KEY_LEFT_ARROW] = true;
		break;
	case GLUT_KEY_RIGHT:
		appState.keyMap[KEY_RIGHT_ARROW] = true;
		break;
	}
	if (glutGetModifiers() == GLUT_ACTIVE_SHIFT)
		appState.keyMap[KEY_SHIFT] = TRUE;
	

	glutPostRedisplay();
	CHECK_GL_ERROR();
}

void specialKeyboardUpCb(int specKeyReleased, int mouseX, int mouseY) {
	switch (specKeyReleased) {
	case GLUT_KEY_UP:
		appState.keyMap[KEY_UP_ARROW] = false;
		break;
	case GLUT_KEY_DOWN:
		appState.keyMap[KEY_DOWN_ARROW] = false;
		break;
	case GLUT_KEY_LEFT:
		appState.keyMap[KEY_LEFT_ARROW] = false;
		break;
	case GLUT_KEY_RIGHT:
		appState.keyMap[KEY_RIGHT_ARROW] = false;	
		break;
	}
	if (glutGetModifiers() != GLUT_ACTIVE_SHIFT)
		appState.keyMap[KEY_SHIFT] = false;

	glutPostRedisplay();
	CHECK_GL_ERROR();
	
} // key released

// -----------------------  Mouse ---------------------------------
// three events - mouse click, mouse drag, and mouse move with no button pressed

// 
/**
 * \brief React to mouse button press and release (mouse click).
 * When the user presses and releases mouse buttons in the window, each press
 * and each release generates a mouse callback (including release after dragging).
 *
 * \param buttonPressed button code (GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, or GLUT_RIGHT_BUTTON)
 * \param buttonState GLUT_DOWN when pressed, GLUT_UP when released
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void mouseCb(int buttonPressed, int buttonState, int mouseX, int mouseY) {

}

/**
 * \brief Handle mouse dragging (mouse move with any button pressed).
 *        This event follows the glutMouseFunc(mouseCb) event.
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void mouseMotionCb(int mouseX, int mouseY) {

}

/**
 * \brief Handle mouse movement over the window (with no button pressed).
 * \param mouseX mouse (cursor) X position
 * \param mouseY mouse (cursor) Y position
 */
void passiveMouseMotionCb(int mouseX, int mouseY) {

	//elevation
	mouseRedisplay = true;
	bool y_changed = false, x_changed = false;
	if (mouseY != appState.window_height / 2) {
		y_changed = true;
		float cameraElevationAngleDelta = MOUSE_SPEED * (mouseY - appState.window_height / 2);
		if (fabs(camera.elevationAngle - cameraElevationAngleDelta) < CAMERA_ELEVATION_MAX)
			camera.elevationAngle -= cameraElevationAngleDelta;
	}
	//rotation
	if (mouseX != appState.window_width / 2) {
		x_changed = true;
		float cameraRotationAngleDelta = MOUSE_SPEED * (mouseX - appState.window_width / 2);
		camera.rotateAngle -= cameraRotationAngleDelta;
		
	}
	bool key_pressed = false;

	//rediplay if sth changed
	if ((y_changed || x_changed)) {
		
		glutWarpPointer(appState.window_width / 2, appState.window_height / 2);
		glutPostRedisplay();

	}
	mouseRedisplay = false;
	
}

// -----------------------  Timer ---------------------------------

/**
 * \brief Callback responsible for the scene update.
 */
void timerCb(int)
{

	
	// update the application state
	appState.time_elapsed = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

	if (rootNode_p) {
		rootNode_p->update(appState.time_elapsed);
	}

	if (appState.keyMap[KEY_RIGHT_ARROW] == true) {
		camera.turnRight();
	}
	if (appState.keyMap[KEY_LEFT_ARROW] == true) {
		camera.turnLeft();
	}
	if (appState.keyMap[KEY_UP_ARROW] == true) {
		camera.goForward();
	}
	if (appState.keyMap[KEY_DOWN_ARROW] == true) {
		camera.goBackward();
	}
	bool key_pressed = false;
	for (auto& key : appState.keyMap)
		if (key)
			key_pressed = true;

	if (key_pressed && appState.keyMap[KEY_SHIFT])
		camera.delta = CAMERA_FASTER_VIEW_SPEED_DELTA;
	else
		camera.delta = CAMERA_VIEW_SPEED_DELTA;

	


	// and plan a new event
	glutTimerFunc(33, timerCb, 0);
	if( !mouseRedisplay)
		glutPostRedisplay();
}



// -----------------------  Application ---------------------------------

/**
 * \brief Initialize application data and OpenGL stuff.
 */
void initApplication() {

	initializeScene();
	// init OpenGL
	
	// - all programs (shaders), buffers, textures, ...
	// init your Application
	
	// - setup the initial application state
	//glCullFace(GL_BACK);
	//glEnable(GL_CULL_FACE); // draw front faces only
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
}

/**
 * \brief Delete all OpenGL objects and application data.
 */
void finalizeApplication(void) {

	// cleanUpObjects();

	// delete buffers
	// cleanupModels();

	// delete shaders
	// cleanupShaderPrograms();
}

/**
 * \brief Entry point of the application.
 * \param argc number of command line arguments
 * \param argv array with argument strings
 * \return 0 if OK, <> elsewhere
 */
int main(int argc, char** argv) {
	restart();
	// initialize the GLUT library (windowing system)
	glutInit(&argc, argv);

	glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

	// for each window
	{
		//   initial window size + title
		glutInitWindowSize(appState.window_width, appState.window_height);
		glutCreateWindow(appState.windowTitle.c_str());

		// Callbacks - use only those you need
		glutDisplayFunc(displayCb);
		glutReshapeFunc(reshapeCb);
		glutKeyboardFunc(keyboardCb);
		glutKeyboardUpFunc(keyboardUpCb);
		glutSpecialFunc(specialKeyboardCb);     // key pressed
		glutSpecialUpFunc(specialKeyboardUpCb); // key released
	
		// glutMouseFunc(mouseCb);
		glutMotionFunc(mouseMotionCb);
		glutTimerFunc(33, timerCb, 0);
	}
	// end for each window 

	// initialize PGR framework (GL, DevIl, etc.)
	if (!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
		pgr::dieWithError("pgr init failed, required OpenGL not supported?");

	// init your stuff - shaders & program, buffers, locations, state of the application
	initApplication();

	// handle window close by the user
	glutCloseFunc(finalizeApplication);

	// Infinite loop handling the events
	glutMainLoop();

	// code after glutLeaveMainLoop()
	// cleanup

	return 0;
}
